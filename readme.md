# Panelle.js - An in-browser comic book panel layout editor

Try it out here:
https://andrewfulrich.gitlab.io/panelle/

## Features so far:

- pan and zoom
- an infinite svg "canvas"
- svg import/export (import is not guaranteed to work on svgs not made in this editor)
- snapping to adjustable grid
- create/edit guide lines
- snapping to guide lines, guide line endpoints, and guide line intersections
- adjustable snapping distance
- adding images into panels
- panning and zooming images within panels
- adjustable stroke
- arrow keys for selection
- adding/deleting corners

# How do I use it?

Click the Help button in the upper right, or just try messing around with the different things to see what they do.

# How do I use this in my workflow?

Well, it's a tool you can use any way you want of course, but notice that after you make your thumbnails, it's fairly easy to use this to make some blank panels in this, which then can be exported as transparent panels you can draw under/over in your favorite painting/drawing program. Or if you like to create/capture each panel image separately, you could upload and position the images into the panels by double-clicking the panels and selecting your image.

Also people might not like the default dimensions/panels, which is understandable and are mostly that way to demonstrate the capabilities, so you might want to make a template page with all the settings you prefer that you can then import whenever you make a new page.

## Post MVP

Features that might be nice to add someday:

- maybe a keyboard shortcut to switch between panels?
- maybe an undo/redo?
- maybe curved panels?

Things to maybe clean up later (sorry the code's a bit of a mess- this was mostly just a for-fun project):

- functions which use+mutate vars declared outside of them
- big, long file with all the js