function app() {
  // grid cell size global and its corresponding html control listeners
  let gridCellSize=25
  document.getElementById('gridSpacing').onchange=e=>gridCellSize=e.target.value
  document.getElementById('gridSpacing').onwheel=e=>{
    e.preventDefault()
    e.stopPropagation()
    gridCellSize=Math.max(gridCellSize+e.deltaY/10,10)
    e.target.value=gridCellSize
  }
  //snapping distance global and its corresponding html control listeners
  let snappingDistance=20
  document.getElementById('snappingDistance').onchange=e=>snappingDistance=e.target.value
  document.getElementById('snappingDistance').onwheel=e=>{
    e.preventDefault()
      e.stopPropagation()
    snappingDistance=Math.max(snappingDistance+e.deltaY/10,1)
    e.target.value=snappingDistance
  }
  //stroke width global and html control listeners
  let strokeWidth=2
  document.getElementById('strokeWidth').onchange=e=>{
    strokeWidth=e.target.value
    Array.from(document.getElementsByTagName('polygon')).forEach(p=>p.setAttribute('stroke-width',strokeWidth))
  }
  document.getElementById('strokeWidth').onwheel=e=>{
    e.preventDefault()
    e.stopPropagation()
    strokeWidth=Math.max(strokeWidth+e.deltaY/100,0)
    e.target.value=strokeWidth
    Array.from(document.getElementsByTagName('polygon')).forEach(p=>p.setAttribute('stroke-width',strokeWidth))
  }
  //an "innerHTML"-like function for svgs
  /** warning: do not pass in "user"-defined string or risk XSS **/
  function appendChildFromString(parent,string) {
    var doc = new DOMParser().parseFromString(
    `<svg xmlns="http://www.w3.org/2000/svg">${string.replaceAll(/>\s*</g,'><').trim()}</svg>`,
    'application/xml');
    doc.documentElement.childNodes.forEach(node=>parent.appendChild(parent.ownerDocument.importNode(node, true)))
  }

  //mode global (which is set by the 3 buttons - pointer, line, and panel)
  let mode="pointer";

  //an incrementing id for created objects
  let idCounter=0;
  function getNextId() {
    return ++idCounter;
  }
  //a global to cache all line intersections. Used when dragging corners so they snap to the intersections.
  let intersections=[]
  function getLines() {
    return Array.from(svgEl.getElementsByTagName('line')).map(l=>[
      [parseInt(l.getAttribute('x1')),parseInt(l.getAttribute('y1'))],
      [parseInt(l.getAttribute('x2')),parseInt(l.getAttribute('y2'))]
    ])
  }

  //a function for debouncing things like scroll/drag listeners
  let requestId;
  //warning: if used by 2 funcs simultaneously, it could cause 1 to be cancelled
  function debounce(func) {
    if(requestId) window.cancelAnimationFrame(requestId);
    requestId=window.requestAnimationFrame(func)
  }

  //a bunch of mutable globals
  let {top,right,bottom,left}=document.body.getBoundingClientRect();
  let zoomFactor=0;
  let dx,dy,svgOffsetX,svgOffsetY;
  let screenWidth=Math.abs(right-left)
  let screenHeight=Math.abs(bottom-top)
  let viewBox=[0,0,screenWidth,screenHeight]

  const ns = 'http://www.w3.org/2000/svg'

  //the main svg element used for editing
  const svgEl=document.createElementNS(ns,'svg')
  svgEl.setAttribute('width',right-left)
  svgEl.setAttribute('height',bottom-top)
  svgEl.setAttribute('viewBox',viewBox.join(' '))
  svgEl.setAttribute('id','editor-svg')
  svgEl.onmousedown=startCanvasDrag
  svgEl.onmousemove=drag
  svgEl.onmouseup=endDrag
  svgEl.onwheel=e=>{
    e.preventDefault()
    e.stopPropagation()
    debounce(()=>scrollZoom(e))
  }
  svgEl.onmouseleave=endDrag
  document.body.prepend(svgEl)
  const defs=document.createElementNS(ns,'defs')
  svgEl.appendChild(defs)
  appendChildFromString(defs,`
    <marker id="dot" markerWidth="10" markerHeight="10" markerUnits="userSpaceOnUse" refX="5" refY="5">
      <circle cx="5" cy="5" r="5" fill="#000" />
    </marker>
    <marker id="biggerDot" markerWidth="6" markerHeight="6" refX="3" refY="3">
      <circle cx="3" cy="3" r="3" />
    </marker>
  `)
  //tickGroup holds the group of grid ticks
  const tickGroup=document.createElementNS(ns,'g')
  //imageGroup groups all images together so they're underneath/over the things they need to be underneath/over
  const imageGroup=document.createElementNS(ns,'g')
  //panelGroup groups all panels together so they're underneath/over the things they need to be underneath/over
  const panelGroup=document.createElementNS(ns,'g')
  //lineGroup groups all lines together so they're underneath/over the things they need to be underneath/over
  const lineGroup=document.createElementNS(ns,'g')
  //circleGroup groups all control points together so they sit on top of everything for easy selection
  const circleGroup=document.createElementNS(ns,'g')
  svgEl.appendChild(tickGroup)
  svgEl.appendChild(imageGroup)
  svgEl.appendChild(panelGroup)
  svgEl.appendChild(lineGroup)
  svgEl.appendChild(circleGroup)
  let selectedEl=null;
  let attachedCallback=null
  let panelBeingCreated=null;
  let lineBeingCreated=null;

  //gotta re-adjust the editor svg to fill the entire window every time the window's resized
  function refreshWindowDimensions() { 
    const dims=document.body.getBoundingClientRect();
    top=dims.top; right=dims.right; bottom=dims.bottom; left=dims.left;
    screenWidth=Math.abs(right-left)
    screenHeight=Math.abs(bottom-top)
    svgEl.setAttribute('width',screenWidth)
    svgEl.setAttribute('height',screenHeight)
    const [offsetX,offsetY,width,height]=svgEl.getAttribute('viewBox').split(' ').map(n=>parseInt(n))
    svgEl.setAttribute('viewBox',`${offsetX} ${offsetY} ${screenWidth*(1.004**zoomFactor)} ${screenHeight*(1.004**zoomFactor)}`)
  }

  window.onresize=()=>debounce(refreshWindowDimensions)

  function scrollZoom(e) {
    e.preventDefault()
    e.stopPropagation()
    const [offsetX,offsetY,width,height]=svgEl.getAttribute('viewBox').split(' ').map(n=>parseInt(n))

    if(zoomFactor-e.deltaY > -1000 && zoomFactor-e.deltaY < 1000) zoomFactor-=e.deltaY;
    
    svgEl.setAttribute('viewBox',`${offsetX} ${offsetY} ${screenWidth*(1.004**zoomFactor)} ${screenHeight*(1.004**zoomFactor)}`)
    const dotRadius=5*(1.004**(zoomFactor))
    scaleDot(document.getElementById('dot'),1)
    scaleDot(document.getElementById('biggerDot'),3/5)
    Array.from(document.querySelectorAll('g > circle')).forEach(d=>scaleCircle(d))
    function scaleDot(dot,scaleFactor) {
      ['cx','cy','r'].forEach(attr=>dot.firstElementChild.setAttribute(attr,scaleFactor*dotRadius))
      dot.setAttribute('markerHeight',scaleFactor*dotRadius*2)
      dot.setAttribute('markerWidth',scaleFactor*dotRadius*2)
      dot.setAttribute('refX',scaleFactor*dotRadius)
      dot.setAttribute('refY',scaleFactor*dotRadius)
    }
    function scaleCircle(circle) {
      circle.setAttribute('r',dotRadius*2)
    }
  }
  
  function startCanvasDrag(e) {
    e.preventDefault()
    e.stopPropagation()
    selectedEl=svgEl
    if(mode=='pointer') {
      const [offsetX,offsetY,width,height]=svgEl.getAttribute('viewBox').split(' ').map(n=>parseInt(n))
      svgOffsetX=offsetX
      svgOffsetY=offsetY
      dx=e.clientX
      dy=e.clientY
    } else if(mode=='line') {
      const [offsetX,offsetY,width,height]=svgEl.getAttribute('viewBox').split(' ').map(n=>parseInt(n))
      const mouseX=Math.round(((e.clientX)*width/screenWidth+offsetX)/gridCellSize)*gridCellSize
      const mouseY=Math.round(((e.clientY)*height/screenHeight+offsetY)/gridCellSize)*gridCellSize
      lineBeingCreated=makeLine(
        mouseX,
        mouseY,
        mouseX+gridCellSize,
        mouseY+gridCellSize
      )
    } else if(mode=='panel') {
      const [offsetX,offsetY,width,height]=svgEl.getAttribute('viewBox').split(' ').map(n=>parseInt(n))
      const mouseX=Math.round(((e.clientX)*width/screenWidth+offsetX)/gridCellSize)*gridCellSize
      const mouseY=Math.round(((e.clientY)*height/screenHeight+offsetY)/gridCellSize)*gridCellSize
      panelBeingCreated=makePanel([
        [mouseX,mouseY],
        [mouseX+gridCellSize,mouseY],
        [mouseX+gridCellSize,mouseY+gridCellSize],
        [mouseX,mouseY+gridCellSize]
      ])
    }
  }

  //an array to hold the grid ticks that appear around the selected control point
  let ticks=[]
  function makeTick(x,y,opacity) {
    const p=document.createElementNS(ns,'polyline')
    p.setAttribute('stroke','#999')
    p.setAttribute('stroke-width',2)
    p.setAttribute('points',[[x,y],[x+gridCellSize/3,y],[x-gridCellSize/3,y],[x,y],[x,y+gridCellSize/3],[x,y-gridCellSize/3]].map(coord=>coord.join(',')).join(' '))
    p.setAttribute('opacity',opacity)
    ticks.push(p)
    tickGroup.appendChild(p)
  }

  function makeTicks(x,y) {
    ticks.forEach(p=>p.remove())
    ticks=[]
    for(let i=-3; i<4; i++) {
      for(let j=-3;j<4;j++) {
        makeTick(x+j*gridCellSize,y+i*gridCellSize,1/(Math.abs(i)+Math.abs(j)+1))
      } 
    }
  }

  function drag(e) {
    const [offsetX,offsetY,width,height]=svgEl.getAttribute('viewBox').split(' ').map(n=>parseInt(n))
    
    if(svgEl==selectedEl) {
      e.preventDefault()
      e.stopPropagation()
      if(mode=='pointer') {
        const newDx=e.clientX
        const newDy=e.clientY
        svgEl.setAttribute('viewBox',`${svgOffsetX-(newDx-dx)*(width/screenWidth)} ${svgOffsetY-(newDy-dy)*(height/viewBox[3])} ${width} ${height}`)
      } else if(mode=='line') {
        const [offsetX,offsetY,width,height]=svgEl.getAttribute('viewBox').split(' ').map(n=>parseInt(n))
        const mouseX=Math.round(((e.clientX)*width/screenWidth+offsetX)/gridCellSize)*gridCellSize
        const mouseY=Math.round(((e.clientY)*height/screenHeight+offsetY)/gridCellSize)*gridCellSize
        lineBeingCreated.pts[1]={id:lineBeingCreated.pts[1].id,coord:[mouseX,mouseY]}
        lineBeingCreated.update()
        lineBeingCreated.updateCircles()
        makeTicks(mouseX,mouseY)
      } else if(mode=='panel' && panelBeingCreated) {
        const [offsetX,offsetY,width,height]=svgEl.getAttribute('viewBox').split(' ').map(n=>parseInt(n))
        const mouseX=Math.round(((e.clientX)*width/screenWidth+offsetX)/gridCellSize)*gridCellSize
        const mouseY=Math.round(((e.clientY)*height/screenHeight+offsetY)/gridCellSize)*gridCellSize
        panelBeingCreated.pts[2]={id:panelBeingCreated.pts[2].id,coord:[mouseX,mouseY]}
        panelBeingCreated.pts[3]={id:panelBeingCreated.pts[3].id,
          coord:[panelBeingCreated.pts[3].coord[0],mouseY]}
        panelBeingCreated.pts[1]={id:panelBeingCreated.pts[1].id,
        coord:[mouseX,panelBeingCreated.pts[1].coord[1]]}
        panelBeingCreated.update()
        panelBeingCreated.updateCircles()
        makeTicks(mouseX,mouseY)
      }
    }
    else if(selectedEl !== null) {
      e.preventDefault()
      e.stopPropagation()
      let newX=e.clientX*width/screenWidth- dx
      let newY=e.clientY*height/screenHeight- dy
      const snapped=selectedEl.snapToLines?snapToLines(getLines(),[newX,newY],snappingDistance,intersections):false
      if(selectedEl.noGridSnap) {
        //do no snapping at all
      } else if(!selectedEl.snapToLines || snapped[0]==newX && snapped[1]==newY) {
        //snap to grid only
        newX=Math.round(newX/gridCellSize)*gridCellSize
        newY=Math.round(newY/gridCellSize)*gridCellSize
        makeTicks(newX,newY)
      } else {
        //snap to grid and lines
        newX=snapped[0]
        newY=snapped[1]
        ticks.forEach(t=>t.setAttribute('opacity',0))
      }
      selectedEl.setAttributeNS(null,selectedX,newX)
      selectedEl.setAttributeNS(null,selectedY,newY)
      if(attachedCallback) attachedCallback(newX,newY);
      if(selectedEl.onclick) selectedEl.dispatchEvent(new Event('click'));
    }
  }

  function endDrag(e) {
    if(svgEl==selectedEl) {
      if(mode=='pointer') {
        selectedEl=null
      } else if(mode=='line') {
        lineBeingCreated=null
        selectedEl=null
      } else if(mode=='panel') {
        panelBeingCreated=null
        selectedEl=null
      }
    }
    else if(selectedEl !== null) {
      selectedEl=null
      attachedCallback=null
    }
  }
  
  //used to make panel corners, lines, etc. draggable
  function makeDraggable(el,elX='x',elY='y',callback) {
    function startDrag(e) {
      e.stopPropagation()
      selectedEl=el
      selectedX=elX
      selectedY=elY
      attachedCallback=callback
      const [offsetX,offsetY,width,height]=svgEl.getAttribute('viewBox').split(' ').map(n=>parseInt(n))
      dx=e.clientX*width/screenWidth-parseInt(el.getAttributeNS(null,selectedX))
      dy=e.clientY*height/screenHeight-parseInt(el.getAttributeNS(null,selectedY))
      el.group.appendChild(el) //make sure the element's always on top
    }
    
    el.isDraggable=true
    el.startDrag=startDrag
    el.addEventListener('mousedown', startDrag)
  }

  function makePoints(coordList) {
    return coordList.map(coord=>coord.join(',')).join(' ')
  }
  function makePanel(coordList) {
    function makeRawPanel() {
      const panel=document.createElementNS(ns,'polygon')
      panel.setAttribute('points',makePoints(coordList))
      panel.setAttribute('stroke','#000')
      panel.setAttribute('stroke-width',strokeWidth)
      panel.setAttribute('marker-end','url(#dot)')
      panel.setAttribute('marker-mid','url(#dot)')
      panel.setAttribute('marker-start','url(#dot)')
      panel.setAttribute('fill','#000')
      panel.setAttribute('fill-opacity',0.5)
      panel.group=panelGroup
      return panel
    }
    const panel=makeRawPanel()
    let panelId=getNextId()
    panel.setAttribute('id',panelId)
    const clipPanel=makeRawPanel()
    const clipPath=document.createElementNS(ns,'clipPath')
    const clipPathId='clipPath'+getNextId()
    clipPath.setAttribute('id',clipPathId)
    panel.clipPathId=clipPathId
    clipPath.appendChild(clipPanel)
    defs.appendChild(clipPath)
    panel.update=()=>{
      panel.setAttribute('points',makePoints(panel.pts.map(p=>p.coord)))
      clipPanel.setAttribute('points',makePoints(panel.pts.map(p=>p.coord)))
    }
    panelGroup.appendChild(panel)
    panel.pts=[]

    panel.ondblclick=()=>{
      if(Array.from(panel.classList).includes('has-image')) return false;
      const fileDialog=document.createElement('input')
      fileDialog.setAttribute('type','file')
      fileDialog.onchange=e=>{
        const file=e.target.files[0]
        const reader= new FileReader()
        reader.onload=()=> {
          makeImage(reader.result, panel)
        }
        file && reader.readAsDataURL(file);
      }
      fileDialog.click();
      
    }

    //if in line or panel mode, this will prevent a line/panel from being created on top of a panel when the panel is clicked/doubleclicked, since that was kind of annoying
    panel.onclick=e=>{
      e.stopPropagation()
      document.getElementById(panel.pts[0].id).dispatchEvent(new Event('click'));
    }
    panel.onmousedown=e=>e.stopPropagation()

    coordList.map((coord,index)=>
      makeControlCircle(coord,panel,(newX,newY,circleId)=>{
        const i=panel.pts.findIndex(p=>p.id==circleId)
        panel.pts[i]={id:circleId,coord:[newX,newY]}
        panel.update()
      },true))
    panel.updateCircles=()=>{
      const circles=panel.pts.map(pt=>document.getElementById(pt.id))
      circles.forEach((c,i)=>{
        c.setAttribute('cx',panel.pts[i].coord[0])
        c.setAttribute('cy',panel.pts[i].coord[1])
      })
    }
    document.getElementById(panel.pts[2].id).dispatchEvent(new Event('click'));
    
    return panel;
  }
  function makeImage(url, panel, existingX=null,existingY=null,existingWidth=null,existingHeight=null) {
    const tempImg = new Image()
    tempImg.src = url
    tempImg.onload = () => {
      const minX = Math.min.apply(null, panel.pts.map(p => p.coord[0]))
      const minY = Math.min.apply(null, panel.pts.map(p => p.coord[1]))
      const maxX = Math.max.apply(null, panel.pts.map(p => p.coord[0]))
      const maxY = Math.max.apply(null, panel.pts.map(p => p.coord[1]))
      //set the size so that it fills the clip mask
      const originalWidth = tempImg.width > tempImg.height ? (maxX - minX) : (maxY - minY) * tempImg.width / tempImg.height
      const originalHeight = tempImg.width > tempImg.height ? (maxX - minX) * tempImg.height / tempImg.width : (maxY - minY)
      const originalX = existingX !== null ? existingX : minX
      const originalY = existingY !== null ? existingY : minY
      const img = document.createElementNS(ns, 'image')
      img.setAttribute('width', existingWidth || originalWidth)
      img.setAttribute('height', existingHeight || originalHeight)
      img.setAttribute('x', originalX)
      img.setAttribute('y', originalY)
      img.setAttribute('href', url)
      img.setAttribute('preserveAspectRatio', 'xMinYMin meet')
      img.setAttribute('clip-path', `url(#${panel.clipPathId})`)
      img.snapToLines = false
      img.noGridSnap = true
      imageGroup.appendChild(img)
      img.group=imageGroup
      makeDraggable(img, 'x', 'y', () => {
        panel.group.appendChild(panel) //put the panel on top of the other panels
        panel.pts.forEach(p => circleGroup.appendChild(document.getElementById(p.id))) //put the panel's control points on top
      })
      zoom = 0
      img.onwheel = e => {
        e.preventDefault()
        e.stopPropagation()
        if (zoom - e.deltaY > -1000 && zoom - e.deltaY < 1000)
          zoom += e.deltaY

        img.setAttribute('height', originalHeight * (1.004 ** zoom))
        img.setAttribute('width', originalHeight * (1.004 ** zoom) * tempImg.width / tempImg.height)
      }
      img.onclick = e => {
        Array.from(document.getElementsByClassName('selected')).forEach(el => el.classList.remove('selected'))
        Array.from(document.getElementsByClassName('related')).forEach(c => c.classList.remove('related'))
        Array.from(document.getElementsByClassName('selected-panel')).forEach(el => el.classList.remove('selected-panel'))
        panel.classList.add('selected-panel')
      }
      panel.img = img
      panel.classList.add('has-image')
      panel.group.appendChild(panel) //put the panel on top of the other panels
      panel.pts.forEach(p => circleGroup.appendChild(document.getElementById(p.id))) //put the panel's control points on top
      img.dispatchEvent(new CustomEvent('click'))
    }
  }

  function makeControlCircle(coord,puppetEl,dragCallback,snapToLines=false,insertIndex=null) {
    const circle=document.createElementNS(ns,'circle')
    circle.setAttribute('cx',coord[0])
    circle.setAttribute('cy',coord[1])
    circle.setAttribute('r',10)
    circle.setAttribute('fill','#000')
    circle.setAttribute('fill-opacity','0')
    let id=getNextId()
    circle.puppetId=puppetEl.getAttribute('id')
    circle.setAttribute('id',id)
    circle.snapToLines=snapToLines
    circle.group=circleGroup
    circleGroup.appendChild(circle) //put this circle on top
    if(insertIndex) puppetEl.pts.splice(insertIndex,0,{id,coord})
    else puppetEl.pts.push({id,coord})
    
    circle.onclick=(e)=>{
      Array.from(document.getElementsByClassName('selected')).forEach(c=>c.classList.remove('selected'))
      Array.from(document.getElementsByClassName('related')).forEach(c=>c.classList.remove('related'))
      Array.from(document.getElementsByClassName('selected-panel')).forEach(c=>c.classList.remove('selected-panel'))
      e.target.classList.add('selected')
      puppetEl.pts
        .map(pt=>document.getElementById(pt.id))
        .filter(ptEl=>!Array.from(ptEl.classList).includes('selected'))
        .forEach(ptEl=>ptEl.classList.add('related'))
    }
    makeDraggable(circle,'cx','cy',(newX,newY)=>dragCallback(newX,newY,id))
    return circle
  }
  function makePage(coordList) {
    const page=document.createElementNS(ns,'polygon')
    page.setAttribute('points',makePoints(coordList))
    page.setAttribute('stroke','#000')
    page.setAttribute('stroke-width',2)
    page.setAttribute('fill','#FFF')
    page.setAttribute('id','page')
    svgEl.prepend(page)
    document.getElementById('pageWidth').onchange=e=>{
      const newCoordList=coordList.slice()
      newCoordList[1][0]=coordList[0][0]+parseInt(e.target.value)
      newCoordList[2][0]=coordList[0][0]+parseInt(e.target.value)
      page.setAttribute('points',makePoints(newCoordList))
    }
    document.getElementById('pageWidth').onwheel=e=>{
      e.preventDefault()
      e.stopPropagation()
      let newVal=Math.max(1,e.deltaY+parseInt(e.target.value))
      const newCoordList=coordList.slice()
      newCoordList[1][0]=coordList[0][0]+newVal
      newCoordList[2][0]=coordList[0][0]+newVal
      e.target.value=newVal
      page.setAttribute('points',makePoints(newCoordList))
    }
    document.getElementById('pageHeight').onchange=e=>{
      e.preventDefault()
      e.stopPropagation()
      const newCoordList=coordList.slice()
      newCoordList[2][1]=coordList[0][1]+parseInt(e.target.value)
      newCoordList[3][1]=coordList[0][1]+parseInt(e.target.value)
      page.setAttribute('points',makePoints(newCoordList))
    }
    document.getElementById('pageHeight').onwheel=e=>{
      e.preventDefault()
      e.stopPropagation()
      const newVal=Math.max(1,e.deltaY+parseInt(e.target.value))
      const newCoordList=coordList.slice()
      newCoordList[2][1]=coordList[0][1]+newVal
      newCoordList[3][1]=coordList[0][1]+newVal
      e.target.value=newVal
      page.setAttribute('points',makePoints(newCoordList))
    }
  }

  function makeLine(x1,y1,x2,y2) {
    const line=document.createElementNS("http://www.w3.org/2000/svg","line");
    line.setAttribute('x1',x1)
    line.setAttribute('y1',y1)
    line.setAttribute('x2',x2)
    line.setAttribute('y2',y2)
    line.setAttribute('stroke-width',strokeWidth)
    line.setAttribute('stroke','#000')
    line.setAttribute('stroke-dasharray','4 2')
    line.setAttribute('fill','none')
    line.setAttribute('marker-end','url(#biggerDot)')
    line.setAttribute('marker-start','url(#biggerDot)')
    line.pts=[]
    line.update=()=>{
      line.setAttribute('x1',line.pts[0].coord[0])
      line.setAttribute('y1',line.pts[0].coord[1])
      line.setAttribute('x2',line.pts[1].coord[0])
      line.setAttribute('y2',line.pts[1].coord[1])
      intersections=findIntersections(getLines())
    }
    let lineId=getNextId()
    line.setAttribute('id',lineId)
    intersections=findIntersections(getLines())
    lineGroup.appendChild(line)
    line.group=lineGroup
    const circles=[]
    circles[0]=makeControlCircle([x1,y1],line,(newX,newY,circleId)=>{
      const i=line.pts.findIndex(p=>p.id==circleId)
      line.pts[i]={id:circleId,coord:[newX,newY]}
      line.update()
    })
    circles[1]=makeControlCircle([x2,y2],line,(newX,newY,circleId)=>{
      const i=line.pts.findIndex(p=>p.id==circleId)
      line.pts[i]={id:circleId,coord:[newX,newY]}
      line.update()
    })
    line.updateCircles=()=>{
      circles.forEach((c,i)=>{
        c.setAttribute('cx',line.pts[i].coord[0])
        c.setAttribute('cy',line.pts[i].coord[1])
      })
    }
    circles[1].dispatchEvent(new Event('click'));
    return line
  }

  makePage([[50,50],[600,50],[600,900],[50,900]])
  makePanel([[75,75],[375,75],[225,875],[75,875]])
  makePanel([[400,75],[575,75],[575,400],[339.0625,400]])
  makePanel([[334.375,425],[575,425],[575,875],[250,875]])

  makeLine(400,75,250,875)
  makeLine(575,400,250,400)
  makeLine(575,425,250,425)

  function loadFromFile(onchangeEvent) {
    var f = onchangeEvent.target.files[0];
    var r = new FileReader();
    r.onload = async function () {
      const data= r.result
      var doc = new DOMParser().parseFromString(data, "image/svg+xml")
      const images=Array.from(doc.querySelectorAll('image'))
      const page=doc.querySelector('polygon[mask="url(#pageClipPath)"]')
      const pagePanels=Array.from(doc.querySelectorAll('#pageClipPath polygon[fill="#000"]'))
      document.getElementById('page').remove()
      makePage(makeCoordListFromPoints(page.getAttribute('points')))
      function makeCoordListFromPoints(pointString) {
        return pointString.split(' ').map(coordString=>coordString.split(',').map(coord=>parseInt(coord)))
      }
      //using remove function on circles will remove all control points which will remove everything associated with those control points
      let elToRemove;
      while(elToRemove=document.querySelector('g > circle')) {
        remove(elToRemove)
      }

      images.forEach(img=>{
        const importedPanel=doc.getElementById(img.getAttribute('clip-path').replace('url(#','').replace(')','')).firstElementChild
        console.log(importedPanel)
        
        const coordList=makeCoordListFromPoints(importedPanel.getAttribute('points'))
        console.log('coordList is: ',coordList,' from ',makeCoordListFromPoints(importedPanel.getAttribute('points')))
        const panel=makePanel(coordList)
        makeImage(
          img.getAttribute('xlink:href'),
          panel,
          parseInt(img.getAttribute('x')),
          parseInt(img.getAttribute('y')),
          parseInt(img.getAttribute('width')),
          parseInt(img.getAttribute('height')))
      })
      
      pagePanels.forEach(importedPanel=>makePanel(makeCoordListFromPoints(importedPanel.getAttribute('points'))))
      strokeWidth=parseInt(page.getAttribute('stroke-width'))
      Array.from(document.getElementsByTagName('polygon')).forEach(p=>p.setAttribute('stroke-width',strokeWidth))

      const storedGridCellSize=doc.firstElementChild.getAttribute('panellejs:grid-spacing')
      if(storedGridCellSize) {
        gridCellSize=parseInt(storedGridCellSize)
        document.getElementById('gridSpacing').value=gridCellSize
      }

      const storedSnappingDistance=doc.firstElementChild.getAttribute('panellejs:snapping-distance')
      if(storedSnappingDistance) {
        snappingDistance=parseInt(storedSnappingDistance);
        document.getElementById('snappingDistance').value=snappingDistance
      }
    }
    r.readAsText(f);
  }
  document.getElementById('loadInput').onchange=loadFromFile

  function remove(el) {
    if(!el) return;
    if(el.isDraggable) el.removeEventListener('mousedown',el.startDrag);
    if(el.onclick) el.removeEventListener('click',el.onclick)
    if(el.onwheel) el.removeEventListener('click',el.onwheel)
    if(el.puppetId) {
      let puppet=document.getElementById(el.puppetId)
      if(puppet.tagName=='polygon') {
        if(puppet.pts.length<=3) {
          puppet.pts.forEach(p=>document.getElementById(p.id).remove())
          puppet.remove()
          if(puppet.clipPathId && document.getElementById(puppet.clipPathId)) document.getElementById(puppet.clipPathId).remove()
          return;
        }
        
        puppet.pts=puppet.pts.filter(p=>p.id !== parseInt(el.getAttribute('id')))
        puppet.update()
        const newSelected=document.getElementById(puppet.pts[0].id)
        newSelected.classList.remove('related')
        newSelected.classList.add('selected')
        puppet.removeEventListener('dblclick',puppet.ondblclick)
      } else if(puppet.tagName=='line') {
        puppet.pts.forEach(p=>document.getElementById(p.id).remove())
        puppet.remove()
      }
      ticks.forEach(t=>t.setAttribute('opacity',0))
    }

    el.remove()
  }

  function captureKeystroke(e) {
    const c=document.querySelector('circle.selected')
    switch(e.keyCode) {
      case 46: //delete
      case 8: //backspace
        //remove a line/panel/image
        remove(document.getElementsByClassName('selected')[0])
        const selectedPanel=document.getElementsByClassName('selected-panel')[0]
        if(selectedPanel) {
          remove(selectedPanel.img)
          selectedPanel.classList.remove('selected-panel')
          selectedPanel.classList.remove('has-image')
        }
        break;
      case 13: //enter
      case 32: //space
        //add another corner to the selected panel if a panel is selected
        if(c && c.puppetId) {
          let puppet=document.getElementById(c.puppetId)
          if(puppet.tagName=='polygon') {
            //find selectedEl using its id in panel.pts
            const index=puppet.pts.findIndex(pt=>pt.id==parseInt(c.getAttribute('id')))
            const prevIndex=index == 0 ? puppet.pts.length-1 : index-1;
            //make a point at the midpoint between this and the previous point
            const newC=makeControlCircle(
              [(puppet.pts[index].coord[0]+puppet.pts[prevIndex].coord[0])/2,
              (puppet.pts[index].coord[1]+puppet.pts[prevIndex].coord[1])/2],
              puppet,
              (newX,newY,circleId)=>{
                const i=puppet.pts.findIndex(p=>p.id==circleId)
                puppet.pts[i]={id:circleId,coord:[newX,newY]}
                puppet.update()
              },
              true,
              index
            )
            newC.classList.add('related')
            puppet.update()
          }
        }
        break;
      case 37: //left arrow
      case 40: //down arrow
        //select the adjacent corner
        if(c && c.puppetId) {
          const puppet=document.getElementById(c.puppetId)
          const index=puppet.pts.findIndex(pt=>pt.id==parseInt(c.getAttribute('id')))
          const prevIndex=index == 0 ? puppet.pts.length-1 : index-1;
          c.classList.remove('selected')
          c.classList.add('related')
          const newSelected= document.getElementById(puppet.pts[prevIndex].id)
          newSelected.classList.remove('related')
          newSelected.classList.add('selected')
        }
        break;
      case 39: //right arrow
      case 38: //up arrow
        //select the other adjacent corner
        if(c && c.puppetId) {
          const puppet=document.getElementById(c.puppetId)
          const index=puppet.pts.findIndex(pt=>pt.id==parseInt(c.getAttribute('id')))
          const nextIndex= index==puppet.pts.length-1 ? 0 : index+1;
          c.classList.remove('selected')
          c.classList.add('related')
          const newSelected= document.getElementById(puppet.pts[nextIndex].id)
          newSelected.classList.remove('related')
          newSelected.classList.add('selected')
        }
        break;
    }
  }
  document.addEventListener('keyup',captureKeystroke)


  const pointerModeEl=document.getElementById('pointerMode')
  const lineModeEl=document.getElementById('lineMode')
  const panelModeEl=document.getElementById('panelMode')
  const previewModeEl=document.getElementById('previewMode')
  const saveBtn=document.getElementById('saveBtn')
  const backToEditEl=document.getElementById('backToEdit')
  const loadLabel=document.getElementById('loadLabel')

  const toggleTheseTogether=[svgEl,dragClickBehavior,previewModeEl,loadLabel].concat(['snappingDistanceLabel','gridSpacingLabel','strokeLabel','pageWidthLabel','pageHeightLabel'].map(id=>document.getElementById(id)))

  //adds a click listener to the button so that it toggles between pointer mode, panel creation mode, etc.
  function makeModeListener(modeSelection,el) {
    el.addEventListener('click',e=> {
      [pointerModeEl,lineModeEl,panelModeEl].forEach(el=>el.classList.remove('selected-mode'))
      mode=modeSelection
      if(mode!=='edit' && mode!=='preview') el.classList.add('selected-mode')
      const previewEl=document.getElementById('preview')
      if(mode=='preview' && !Array.from(svgEl.classList).includes('hidden')) {
        document.body.classList.add('preview-body')
        const pageEl=document.getElementById('page')
        const pageCoords=pageEl.getAttribute('points').split(' ').map(s=>s.split(',').map(n=>parseInt(n)))
        const width=pageCoords[1][0]-pageCoords[0][0]
        const height=pageCoords[2][1]-pageCoords[0][1]
        const images=Array.from(svgEl.querySelectorAll('image'))
        const clipPathIds=images.map(img=>img.getAttribute('clip-path'))
        const clipPaths=Array.from(svgEl.querySelectorAll('clipPath'))
        const clipPathsWithImages=clipPaths.filter(cp=>clipPathIds.includes(`url(#${cp.getAttribute('id')})`))
        const clipPathsWithoutImages=clipPaths.filter(cp=>!clipPathIds.includes(`url(#${cp.getAttribute('id')})`))
        appendChildFromString(previewEl,`
        <svg xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:panellejs="http://gitlab.com/andrewfulrich/panelle" panellejs:grid-spacing="${gridCellSize}" panellejs:snapping-distance="${snappingDistance}" id="previewSVG" width="${width}" height="${height}" viewBox="${`50 50 ${width} ${height}`}">
          <defs id="defs">
            ${clipPathsWithImages.map(cp=>`<clipPath id="new${cp.getAttribute('id')}">
              <polygon fill="#000" points="${cp.firstElementChild.getAttribute('points')}" ></polygon></clipPath>`)}
              <mask id="pageClipPath">
                <polygon points="${pageEl.getAttribute('points')}"
                fill="#FFF"
                ></polygon>
                ${clipPathsWithoutImages.map(cp=>cp.firstElementChild).map(p=>`
                  <polygon points="${p.getAttribute('points')}" fill="#000"></polygon>`)}
              </mask>
          </defs>
          <polygon points="${pageEl.getAttribute('points')}"
            mask="url(#pageClipPath)"
            fill="#FFF"
            stroke="#000"
            stroke-width="${strokeWidth}"
            ></polygon>
            ${images.map(img=>`<image 
            width="${img.getAttribute('width')}"
            height="${img.getAttribute('height')}"
            xlink:href="${img.getAttribute('href')}"
            x="${img.getAttribute('x')}"
            y="${img.getAttribute('y')}"
            clip-path="url(#new${img.getAttribute('clip-path').replace('url(#','')}"
            preserveAspectRatio="xMinYMin meet"
            ></image>
            `)}
            ${clipPaths.map(cp=>cp.firstElementChild).map(p=>`
                  <polygon 
                  stroke="#000"
                  stroke-width="${strokeWidth}"
                  points="${p.getAttribute('points')}" 
                  fill="none"></polygon>`)}
          </svg>
        `)
        saveBtn.classList.remove('hidden')
        backToEditEl.classList.remove('hidden')
        toggleTheseTogether.forEach(el=>el.classList.add('hidden'))
      } else if(mode =='edit') {
        document.body.classList.remove('preview-body')
        saveBtn.classList.add('hidden')
        backToEditEl.classList.add('hidden')
        if(previewEl.firstElementChild) previewEl.firstElementChild.remove()
        toggleTheseTogether.forEach(el=>el.classList.remove('hidden'))
        pointerModeEl.click()
      }
    })
  }

  makeModeListener('pointer',pointerModeEl)
  makeModeListener('line',lineModeEl)
  makeModeListener('panel',panelModeEl)
  makeModeListener('preview',previewModeEl)
  makeModeListener('edit',backToEditEl)

  saveBtn.addEventListener('click',()=>{
    if(mode =='preview') {
      const filename=window.prompt('Enter Filename')
      if(filename && filename.trim().length>0) {
        const a = document.createElement("a");
        document.body.appendChild(a);
        //get svg element.
        const svg = document.getElementById("previewSVG");
        //get svg source.
        const serializer = new XMLSerializer();
        let source = serializer.serializeToString(svg);
        
        //add xml declaration
        source = '<?xml version="1.0" standalone="no"?>\r\n' + source;

        //convert svg source to URI data scheme.
        a.href = "data:image/svg+xml;charset=utf-8,"+encodeURIComponent(source);
        a.download = filename.replace('.svg','')+'.svg'
        a.onclick = function () {
          setTimeout(function () {
            window.URL.revokeObjectURL(a.href);
            a.remove()
          }, 1500);
        };
        a.click();
      }
    }
  })
}

app()